/**
 * Created by db on 15/11/30.
 */
(function($){
    $(function(){
        BestAsideNav();
    });
})(jQuery);

function BestAsideNav(){
    $('.aside-nav-menu').click(function(){
        var $this = $(this), $list = $this.next('.aside-nav-list-container');

        if($list.length > 0 ){
            if($list.is(':hidden')){
                $list.slideDown();
            }else{
                $list.slideUp();
            }
        }
    });
}